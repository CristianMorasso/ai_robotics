import math
import time

import numpy as np

import copy 
from nav_msgs.msg import Odometry
import rclpy
from geometry_msgs.msg import Twist, Point
from rclpy.qos import QoSProfile
from sensor_msgs.msg import LaserScan
import rclpy.qos
import tf_transformations
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener

from math import pi

class TurtleBot3():

    def __init__(self, goal):
        
        
        #qos = QoSProfile(depth=10)
        # self.node = rclpy.create_node('turtlebot3_DDQN_node')
        self.lidar_msg = LaserScan()
        self.odom_msg = Odometry()
        self.enemy_odom_msg = Odometry()
        # set your desired goal: 
        self.goal_x, self.goal_y = goal # this is for simulation change for real robot

        # linear velocity is costant set your value
        self.linear_velocity = 0.2  # to comment
        
        # ang_vel is in rad/s, so we rotate 5 deg/s [action0, action1, action2]
        self.angular_velocity = [0.0, -2.20, 2.20]# to comment 
        self.distanceNormFact = 3

        # self.r = rclpy.spin_once(self.node,timeout_sec=0.25)

        
        print("Robot initialized")

    def SetLaser(self, msg):
        self.lidar_msg = msg

    def SetOdom(self, msg):
        self.odom_msg = msg

    def SetEnemyOdom(self, msg):
        self.enemy_odom_msg = msg

    def stop_tb(self):
        self.pub.publish(Twist())


    

    def get_odom(self):
        
        # read odometry pose from self.odom_msg (for domuentation check http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/Odometry.html)
        # save in point variable the position
        # save in rot variable the rotation
        
        #x Unity =  - y pos
        #z Unity = x pos
        point_old = self.odom_msg.pose.pose.position
        point = copy.deepcopy(point_old)
        point.x = -point_old.y
        point.y = 0.0
        point.z = point_old.x
        rot = self.odom_msg.pose.pose.orientation

        self.rot_ = tf_transformations.euler_from_quaternion([rot.x, rot.y, rot.z, rot.w])
        
        return point, (1 + (np.rad2deg(self.rot_[2]) / 180)) / 2


    def get_enemy_distance(self):
        point_old = self.odom_msg.pose.pose.position
        point = copy.deepcopy(point_old)
        point.x = -point_old.y
        point.y = 0.0
        point.z = point_old.x

        enemy_point_old = self.enemy_odom_msg.pose.pose.position
        enemy_point = copy.deepcopy(enemy_point_old)
        enemy_point.x = -enemy_point_old.y
        enemy_point.y = 0.0
        enemy_point.z = enemy_point_old.x
        print("ENEMY POS: ",enemy_point)
        distance = np.linalg.norm(np.array([point.x, point.z])-np.array([enemy_point.x, enemy_point.z]))
        distance = distance /self.distanceNormFact

        return distance

    def get_scan(self):
        #return [1.,1.,1.,1.,1.,1.,1.]
        ranges = []
        scan_val = []

        # read lidar msg from self.lidar_msg and save in scan variable
        len_ranges = len(self.lidar_msg.ranges)
        angle_min = self.lidar_msg.angle_min
        angle_max = self.lidar_msg.angle_max
        angle_increment = self.lidar_msg.angle_increment


        for i in range(len_ranges):       # cast limit values (0, Inf) to usable floats
            if self.lidar_msg.ranges[i] == float('Inf') or math.isnan(self.lidar_msg.ranges[i]) or self.lidar_msg.ranges[i] > 1.0 :
                self.lidar_msg.ranges[i] = 1.0
            #self.lidar_msg.ranges[i] /= 5.5

        # get the rays like training if the network accept 3 (7) rays you have to keep the same number
        # I suggesto to create a list of desire angles and after get the rays from the ranges list  
        # save the result in scan_val list

        for (i, range_record) in enumerate(self.lidar_msg.ranges):
            ranges.append({
                "angle": angle_min + (i * angle_increment) if (i < len_ranges / 2) else angle_min + (i * angle_increment) - 6.28,
                "value": range_record
            })

        
        for i in range(7):
            angle = ((i - 3)*30)%360
            rng_min = angle - 15
            rng_max = angle + 15
            rng = ranges[rng_min: rng_max]
            if (angle == 0):
                rng = ranges[rng_min:] + ranges[:rng_max]
            value = self.get_min(rng)
            scan_val.append(value)
            scan_val = scan_val[::-1]

        return scan_val
    
    def get_min(self, ranges):
        min_value = ranges[0]["value"] if len(ranges) > 0 else 1.0
        for range in ranges:
            min_value = min(min_value, range["value"])
        return min_value

    def get_goal_info(self, tb3_pos):

        # compute distance euclidean distance use self.goal_x/y pose and tb3_pose.x/y
        # compute the heading using atan2 of delta y and x
        # subctract the actual robot rotation to heading
        # save in distance and heading the value
        

        distance = np.linalg.norm(np.array([self.goal_x, self.goal_y])-np.array([tb3_pos.x, tb3_pos.z]))
        
        
        yaw = self.pi_domain(np.rad2deg(self.rot_[2]))
        print("yaw: ", yaw)
        angleInDegrees =   np.rad2deg(np.arctan2(self.goal_y-tb3_pos.z, self.goal_x-tb3_pos.x)) 
        angleInDegrees= self.pi_domain(angleInDegrees-90)
        print("angle deg: ", angleInDegrees)
        heading = -self.pi_domain(angleInDegrees - yaw)
        print(f"heading: {heading}\n")
        print(f"GET GOAL DIST: {distance}, Heading: {heading}")
        
        # we round the distance dividing by 2.8 under the assumption that the max distance between 
        # two points in the environment is approximately 3.3 meters, e.g. 3m
        # return heading in deg
        return distance/self.distanceNormFact, 0.5 + (heading / 360)#(np.rad2deg(heading) / 360)
    
    def pi_domain(self, a):
        #print("A Pre: ", a)
        #modulo
        a = a - int(a/360) * 360
        
        #print("A Post: ", a)
        if a > 180:
            a=  a - 360
            #print("A Neg: ", a)
        if a < -180:
            a=  a + 360
            #print("A Pos: ", a)
        #print("A out: ", a)
        return a
    
    # private float WrapAngleDeg(float angle){
	# 	//returns a [-pi, pi] angle
	# 	//as first we bring it back in a 2pi version
	# 	float angle_360 = angle % 360;
	# 	if (angle_360 > 180){
	# 		angle_360 = 360 - angle_360;
	# 	}
	# 	if (angle_360 < -180){
	# 		angle_360 = angle_360 + 360;
	# 	}
	# 	print("angle_deg"+angle_360);
	# 	return angle_360;
		

	# }
    def move(self, action, pub):
        # stop robot
        if action == -1:
            pub.publish(Twist())
        else:
            # check action 0: move forward 1: turn left 2: turn right
            # save the linear velocity in target_linear_velocity
            # save the angular velocity in target_angular_velocity
            target_linear_velocity = 0.0
            target_angular_velocity = 0.0

            if (action == 0):
                target_linear_velocity = 0.2
                target_angular_velocity = 0.0
            elif (action == 1):
                target_linear_velocity = 0.0
                target_angular_velocity = -2.2
            elif (action == 2):
                target_linear_velocity = 0.0
                target_angular_velocity = 2.2
        
            
            twist = Twist() 
            twist.linear.x = target_linear_velocity
            twist.linear.y = 0.0
            twist.linear.z = 0.0
                            
            twist.angular.x = 0.0
            twist.angular.y = 0.0
            twist.angular.z = target_angular_velocity

            pub.publish(twist)
