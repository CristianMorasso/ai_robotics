using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rclcs;

public class HubSubscriber : MonoBehaviourRosNode
{
    public string NodeName = "Hub_listener";
    public string Topic = "hub_topic";
    
    protected override string nodeName { get { return NodeName; } }
    private Subscription<std_msgs.msg.String> chatterSubscription;

    protected override void StartRos()
    {
        chatterSubscription = node.CreateSubscription<std_msgs.msg.String>(
            Topic,
            (msg) => {
                Debug.Log("I heard: " + msg.Data);
            });
    }
    void Start()
    {
        
    }

    void Update()
    {
       SpinSome(); 
    }
}

