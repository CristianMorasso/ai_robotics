using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float minX; // Coordinata X minima
    public float maxX; // Coordinata X massima
    public float minZ; // Coordinata Z minima
    public float maxZ; // Coordinata Z massima

    private Vector3 targetPosition; // Posizione del punto di destinazione
    public float movementSpeed = 2f; // Velocità di movimento

    int scale = 1;

    private void Start()
    {
        // Genera la prima posizione di destinazione casuale
        GenerateRandomTargetPosition();
    }

    private void Update()
    {
        
        if (Input.GetKeyDown("space")){
            if (scale == 1){
                scale = 2;
            }else{
                scale = 1;
            }
            print("Setting timescale to: " + scale.ToString());
            Time.timeScale = scale;
            
        }

        // Muovi il nemico verso la posizione di destinazione
        MoveTowardsTarget();

        // Controlla se il nemico ha raggiunto la posizione di destinazione
        if (HasReachedTarget())
        {
            // Genera una nuova posizione di destinazione casuale
            GenerateRandomTargetPosition();
        }
    }

    private void MoveTowardsTarget()
    {
        // Calcola la direzione verso la posizione di destinazione
        Vector3 direction = (targetPosition - transform.position).normalized;

        // Ruota il nemico gradualmente nella direzione in cui si sta muovendo
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 40 * Time.deltaTime);

        // Sposta il nemico verso la posizione di destinazione
        transform.position += direction * movementSpeed * Time.deltaTime;
    }

    private bool HasReachedTarget()
    {
        // Controlla se il nemico è sufficientemente vicino alla posizione di destinazione
        float distance = Vector3.Distance(transform.position, targetPosition);
        return distance < 0.1f;
    }

    private void GenerateRandomTargetPosition()
    {
        // Genera una posizione di destinazione casuale all'interno dell'area specificata
        float randomX = Random.Range(minX, maxX);
        float randomZ = Random.Range(minZ, maxZ);
        targetPosition = new Vector3(randomX, 0f, randomZ);
    }
}
