using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rclcs;

public class PositionEmitter : MonoBehaviourRosNode
{

    string NodeName = "robot_emitter_";
    public string Topic = "robot_position_update";

    public float PublishingFrequency = 10.0f;

    protected override string nodeName { get { return NodeName; } }
    private Publisher<std_msgs.msg.String> positionPublisher;
    private std_msgs.msg.String msg;

    public string id;

    protected override void StartRos()
    {
        positionPublisher = node.CreatePublisher<std_msgs.msg.String>(Topic);
        msg = new std_msgs.msg.String();
        StartCoroutine("UpdatePosition");
    }

    private void Start() {
        id = GenerateRandomUUID(48);
        NodeName = "robot_emitter_" + id;
        if (this.GetComponent<PositionsRetriever>()){
            this.GetComponent<PositionsRetriever>().setup(id);
        }
        
        CreateRosNode();
        StartRos();
    }

    private string GenerateRandomUUID(int length){
        const string glyphs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        string code = "";

        for(int i = 0; i < length; i++)
        {
            code += glyphs[Random.Range(0, glyphs.Length)];
        }

        return code;
    }

    IEnumerator UpdatePosition()
    {
        for (;;)
        {
            Vector3 pos = transform.position;
            
            msg.Data = id.ToString() + ";" + pos.x.ToString() + ";" + pos.z.ToString();
            positionPublisher.Publish(msg);
            yield return new WaitForSeconds(1.0f/PublishingFrequency);
        }
    }
}
