using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rclcs;

public class PositionsRetriever : MonoBehaviourRosNode
{
    string NodeName = "robot_subscriber_";
    public string Topic = "robot_position_update";

    private string id;

    protected override string nodeName { get { return NodeName; } }
    private Subscription<std_msgs.msg.String> positionSubscription;

    List<float> distances = new List<float>();
    
    public float shortestDistance;

    protected override void StartRos()
    {
        print("subscribing to " + Topic + " with name " + NodeName);
        positionSubscription = node.CreateSubscription<std_msgs.msg.String>(
            Topic,
            (msg) => {
                
                string[] arrayData =  msg.Data.Split(char.Parse(";"));
                string topic_id = arrayData[0];
                float topic_x = float.Parse(arrayData[1]);
                float topic_z = float.Parse(arrayData[2]);

                if (topic_id.Equals(id)){
                    return;
                }
                
                float distance = Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(topic_x, 0, topic_z));
                shortestDistance = distance;
            });
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public void setup(string id){
        this.id = id;
        NodeName = "robot_subscriber_" + id;
        CreateRosNode();
        StartRos();
    }

    // Update is called once per frame
    void Update()
    {
        SpinSome(); 
    }
}
