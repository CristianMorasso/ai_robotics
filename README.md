# Ai_Robotics
DQN driven turtlebot3 that can reach its goal in a unseen enviroment with mobile obstacles.</br>
This is a small guide on how to make this project work.</br>
- [File Organization](README.md?ref_type=heads#file-system)
- [How it works](README.md?ref_type=heads#how-it-works)
- [Some Results](README.md?ref_type=heads#some-results)
- [Future Works](README.md?ref_type=heads#future-works)
## FILE SYSTEM

At first we divided the files in 4 main directories, plus _imgs_:
  - _MobileRoboticsDQN_: with the code and all files to implement DQN
  - _turtlebot3\_DQN_ : with the implementation on ros, core is in _Ros\_files_
  - _turtlebot3UnityDQN_: with the Unity enviroment for DQN 
  - _turtlebot3UnityROS2_: with the Unity enviroment for the testing with ROS2

In the _turtlebot3\_DQN/Ros\_files_ you can find the implemention in ROS2 to run the project in Unity or in real world (the robots use topics to comunicate).</br>
In particular:
 -  _model\_trained_: here there are some trained policies
 -  _turtlebot3\_DQN_: here there are the implementations of turtlebot3
 -  _setup.py_: the setup file

Then we have _turtlebot3UnityDQN_ with the training files.</br>
In particular _CustomAgent.cs_ (turtlebot3UnityDQN\Assets\Scripts\CustomAgent.cs).
It's the file that sends the observations to our net.

In _MobileRoboticsDQN_ there is _robotic\_navigation.py_ (MobileRoboticsDQN\DQN\env\robotic_navigation.py).
It's the core file for the robot learning, here we override the reward function!


## HOW IT WORKS
### Algorithm and Net
We've used DQN and the net is pretty simple, it has 2 hidden layers with 32 nodes and relu as activation function.</br>
The input and output sizes are taken form the enviroment, we decided to use 10 input and 3 outputs.</br>

**10 Inputs**: 
- 7 values for the lidar 3 left 3 right and 1 middle
- 1 for the heading with the goal (rotation required to point the goal) [0,1] where 0.5 means no rotation needed
- 1 for the distance to the goal, normalized in a [0,1] range (we've divided by a NormFactor)
- 1 for the distance to the other robot(mobile obstacle), normalized in a [0,1] range (we've divided by a NormFactor)
  
**NOTE**: all the input domains are [0,1], for the lidar there is no normalization.

**3 Outputs**: Actions
- "0": forward
- "1": left
- "2": right


### Unity (Train & Test)
Given the environment we have to set up the training phase.

 #### CustomAgent.cs 
 This file sends the Unity observations to our python file _robotic_navigation.py_ that talks to the net.
 </br>
 Below there is the main function that populates the VectorSensor with the observations.
 
```cs

// Listener for the observations collections.
// The observations for the LiDAR sensor are inherited from the 
// editor, in thi function we add the other observations (angle, distance)
public override void CollectObservations(VectorSensor sensor) {	
	
	...
	// Compute orientation  
	float yaw = WrapAngleDeg(-transform.eulerAngles.y);
	// Compute the angle using the built-in function, the function returns a value between -180 and +180
	Vector3 targetDir = target.position - transform.position;
	float angle = Vector3.SignedAngle(targetDir, transform.forward, transform.up);
	//Computing angle from the robot to the goal
	float angleToTarget = Mathf.Atan2(targetPos.y - agentPos.y, targetPos.x - agentPos.x);
	float angleInDegrees = angleToTarget * Mathf.Rad2Deg;
	//Fixing the offset form the 2 angles origins
	angleInDegrees=WrapAngleDeg(angleInDegrees-90);
	//Compute the final Heading
	float angleWithOrientation = 0;
	angleWithOrientation = WrapAngleDeg(angleInDegrees - yaw);
	//Norm
	float angleWithOrientationNorm = 0.5f - (angleWithOrientation / 360f);
	// Add the two observations inside the array of the obseravtions
	sensor.AddObservation( angleWithOrientationNorm );

	...
}
 ```

#### robotic_navigation.py
We've modified the observation space, including the domains for the enemy distance, that is normalized between 0 and 1.
```python
self.observation_space = gym.spaces.Box(
	np.array([0 for _ in range(self.scan_number)] + [0, 0] + [0]),
	np.array([1 for _ in range(self.scan_number)] + [1, 1] + [1]),
	dtype=np.float64
)
```

 #### Reward Function
 One of the main point of a good training is the reward function, we used this one:
  ```python
if not done:
	reward = ((old_distance - new_distance) ) * reward_multiplier - step_penalty
	#process enemy distance
	if enemy_distance < 0.20:
		reward += -(0.2 - enemy_distance) 
elif info["collision"]:
	reward = -10
elif info["goal_reached"]:
	reward = 20
  ```
  We check the distance to the goal if it's increasing or decreasing giving a malus in the first case and a bonus otherwise, then we care about the distance from the _enemy_robot_, giving a negative value if our agent is close to him.

#### Training logs
![Immagine del terminale](img/terminale.png?raw=true)
As we can see from this image, at each iteration the program prints some useful information to the terminal. This information can give us an idea of how much our network is learning.</br>
The image represents the state of the network at a fairly high number of epochs. In fact, it is possible to see how in most of the cases the robot arrived at its destination, with a reward value that exceeds zero. Among the first lines of the logs, we can see a series of negative rewards due to the robot meeting the enemy robot and hitting it.</br>
We can understand this from the very low number of steps, which indicates that the enemy robot was passing through the robot's initial position. Although this network did not reach the threashold of 79 percentage points that enables the saving of the model, we can still say that it was working quite well.

### ROS (testing)
Our two robots are controlled by a program written in python managed by a common class called Turtlebot3 and two different classes that inherit some functions from the common. 

In fact, the two robots differ only in the topics they are subscribed to, the topics they emit, and the goal. Robot 1 subscribes to robot 2's topics and vice versa. Each robot uses the opponent robot's odometry data to calculate its distance and use it in the reward function.
```python
def get_enemy_distance(self):
	point_old = self.odom_msg.pose.pose.position
	point = copy.deepcopy(point_old)
	point.x = -point_old.y
	point.y = 0.0
	point.z = point_old.x
	
	enemy_point_old = self.enemy_odom_msg.pose.pose.position
	enemy_point = copy.deepcopy(enemy_point_old)
	enemy_point.x = -enemy_point_old.y
	enemy_point.y = 0.0
	enemy_point.z = enemy_point_old.x
	
	distance = np.linalg.norm(np.array([point.x, point.z])-np.array([enemy_point.x, enemy_point.z]))
	distance = distance /self.distanceNormFact
	
	return distance
```
The odometry data has been slightly modified, reversing some axes, to perform the calculations having the same axes as in Unity, to facilitate greater understanding during development.

**IMPORTANT** If you use the policy on a ros2 version please be sure that the inputs pre-processing is the same of the training (_CustomAgent.cs_)
## SOME RESULTS 
### Testing on Unity
![Immagine del testing](img/testing.png?raw=true)
Here we can see a portion of the training.py script, that tests the policy (with Unity) in the same environment.</br>
- 72/100 success rate, it's not too bad for a simple reward function
- 11/100 crash rate, it's a bit too high, the network is not good enough to dodge the other robot (check the last value on the list printed every crash, it's small)
- 17/100 time out rate, it means that the robot can not reach the goal and gets stuck, due to the walls,. Near the wall the robot is not well trained so he cannot move or reach the goal.

### Testing on ROS
![gif testing](img/video_demo.gif?raw=true)
Here is shown a small demo of the testing phase with ros. The most interesting part is when the two robots are very close to each other: the left robot manages to move even if it was "stuck" in order to maintain an acceptable distance, after which it manages to unstuck itself/recognize where it is and to move.
## HOW TO RUN
**Training the model**
```console
python3 training.py
```
The training output will be saved in a file with extension ".h5".

**Testing the model**</br>
On Unity:
```console
python3 testing.py
```
With ROS2:
```console
ros2 run turtlebot3_DQN turtlebot3_DQN_1
ros2 run turtlebot3_DQN turtlebot3_DQN_2
```
One command per terminal, to control the two robots in the scene.

### FUTURE WORKS 

- We think that we could achieve better performances with a better train for the policy, training for more epochs and also writing a better reward function that considers the critical point we've discovered (i.e. take more care about the wall and when the goal is near it).
- Another interesting improvement could be adding more inputs to the net, i.e. you could consider the enemy no longer with only the distance but also with the orientation in order to understand where he could move.
- The end the most interesting improvement is extending this work with more then 2 robots, but in this case we will need to change the topics because up to now they are written as _robot1/..._ and _robot2/..._ and are not dynamic.
